using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Twobullets: Bullet {
	public Bullet bala;
	public Bullet bala2;
	public float inb;
	public float inb2;

	/*void OnTriggerEnter2D(Collider2D col){
		if (col.tag == "Finish") {
			bala.shooting = false;
			bala2.shooting = false;
			shooting = false;
			transform.position = new Vector3(0,0,0);
			Reset ();
		}
	}


	protected new virtual void Update(){
		base.Update ();
		if (shooting) {

			bala.speed = 5;
			bala2.speed = 5;
			bala.transform.Translate (0, speed * Time.deltaTime, 0);
			bala2.transform.Translate (0, speed * Time.deltaTime, 0);
		}

	}
*/

	public void Shot(Vector3 position, float direction){
		bala.transform.position = position + new Vector3(inb,0,0) ;
		bala2.transform.position = position + new Vector3(inb2,0,0) ;
		bala.shooting = true;
		bala2.shooting = true;
		bala.transform.rotation = Quaternion.Euler(0, 0, direction);
		bala2.transform.rotation = Quaternion.Euler(0, 0, direction);
	}
}
