﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Smallmeteor : Tinnymeteor {

	protected override void Explode(){

		MeteorManager.getInstance ().LaunchMeteor (3, transform.position, new Vector2 (-4, 4), -5);
		MeteorManager.getInstance ().LaunchMeteor (3, transform.position, new Vector2 (4, 4), 5);
		base.Explode ();
	}
}
