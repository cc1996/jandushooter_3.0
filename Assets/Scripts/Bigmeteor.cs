﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bigmeteor : Tinnymeteor {

	protected override void Explode(){

		MeteorManager.getInstance ().LaunchMeteor (1, transform.position, new Vector2 (-4, -4), -5);
		MeteorManager.getInstance ().LaunchMeteor (1, transform.position, new Vector2 (4, -4), 5);
		base.Explode ();

}

		
		protected void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Tinny"||other.tag == "Bullet") {
				Explode ();
			}
		}
	}