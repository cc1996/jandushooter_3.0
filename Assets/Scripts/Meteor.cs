﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public  class Meteor : MonoBehaviour {

	public bool launched;
	public Vector2 moveSpeed;
	protected float rotationSpeed;
	public ParticleSystem particle;
	public Transform graphics;

	private Collider2D meteorCollider;
	public AudioSource audioexplosion;


	public Vector3 iniPos;

	void Start(){
		iniPos = transform.position;
		meteorCollider = GetComponent<Collider2D> ();

	}

	public void LaunchMeteor(Vector3 position,Vector2 direction,float rotation){
		transform.position = position;
		launched = true;
		transform.rotation = Quaternion.Euler(0, 0, 0);
		moveSpeed = direction;
		graphics.gameObject.SetActive (true);
		rotationSpeed = rotation;
	}

	// Update is called once per frame
	protected void Update () {
		if (launched) {
			transform.Translate (moveSpeed.x*Time.deltaTime, moveSpeed.y * Time.deltaTime, 0);
			transform.GetChild(0).Rotate (0, 0, rotationSpeed );
		}
	}

	protected void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Bullet") {
			Explode ();
		}else if(other.tag == "Finish") {
			Reset ();
		}
	}

	protected void Reset(){
		transform.position = iniPos;
		launched = false;
	}

	protected virtual void Explode(){
		audioexplosion.Play ();
		meteorCollider.enabled = false;
		launched = false;
		graphics.gameObject.SetActive (false);
		particle.Play ();
		Invoke ("Reset", 1);
	}
}